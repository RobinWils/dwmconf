# dwmconf
### What... is this thing?
The package dwmconf will help you with building a custom dwm configuration. It will provide you the dwmconf command. This command will have some parameters which will help you with configuring dwm.  

A tool like this might make it easier for some to move to the dwm windowmanager.

### Projectstatus
This project is currently on hold. 

#### Reasons  
Robin Wils currently uses EXWM. He is still interested in the project but there is no code yet. The team kinda forgot about the project after a busy period. We are still interested in making this project possible if we find out that more people are interested but the project is on hold *for now*.

Feel free to send us a mail if you are interested though.

### License
GNU GPL license  

### Backend
D language.  
Why? Because it's both fast and beautiful.

### Dependencies
No dependencies yet.

### Contact us
mrwils@protonmail.com  
idf31@protonmail.com